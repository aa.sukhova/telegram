#pragma once

#include <ctime>
#include <fstream>
#include <regex>
#include <sys/stat.h>
#include <vector>

class Ctx {
public:
	Ctx();

    void Add(std::string text);

    void Log(std::string text);

    void Rotate();

    void Clean();

private:
    std::string id_;
    std::string log_file_;
    std::vector<std::string> info_;
};
