#pragma once

#include <Poco/Net/HTTPSessionFactory.h>
#include <Poco/Net/HTTPSClientSession.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/URI.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/JSON/Parser.h>

#include "ctx.h"

class Http {

public:
    struct HttpResponse {
        int status;
        std::string error;
        Poco::SharedPtr<Poco::JSON::Object> body;
    };

    HttpResponse Request(Ctx& ctx, std::string url, std::string method,
                         std::map<std::string, std::string> query,
                         std::map<std::string, std::string> hdrs, std::string body);
};
