#pragma once

#include <memory>
#include <fstream>
#include <unordered_map>

#include "ctx.h"
#include "http.h"

class BotClient {
public:
    BotClient(std::string bot_id);

    struct Info {
        std::string username;
        std::string first_name;
        int id;
        bool is_bot;
    };

    struct Message {
        int update_id;
        int from_id;
        int chat_id;
        int message_id;
        std::string text;
        std::string chat_type;
        bool reply;
        int reply_to_id;
    };

    struct Response {
        bool ok;
        int status;
        std::string error;
        std::vector<Message> updates;
        Info info;
    };

    Response GetMe(Ctx& ctx);

    Response GetUpdates(Ctx& ctx, std::unordered_map<std::string, int>);

    Response SendMessage(Ctx& ctx, Message& msg, std::string text);

    int GetOffset();

    void SetOffset(int);

private:
    std::string url_;
    std::fstream offset_file_;
    Http http_;
};
