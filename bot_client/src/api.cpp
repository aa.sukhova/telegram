#include "bot_client/inc/api.h"

BotClient::BotClient(std::string bot_id) : url_("https://api.telegram.org/bot" + bot_id) {
}

BotClient::Response BotClient::GetMe(Ctx& ctx) {
    ctx.Add("GetMe: ");

    auto response = http_.Request(ctx, url_ + "/getMe", "GET", {}, {}, "");

    if (response.status != 200) {
        ctx.Log("end with status " + std::to_string(response.status) +
                ", error: " + response.error);

        return {false, response.status, response.error};
    }

    bool ok;
    Info info;

    try {
        ok = response.body->getValue<bool>("ok");

        if (ok) {
            auto parse_result = response.body->getObject("result");

            info.is_bot = parse_result->getValue<bool>("is_bot");
            info.id = parse_result->getValue<int>("id");
            info.first_name = parse_result->getValue<std::string>("first_name");
            info.username = parse_result->getValue<std::string>("username");
        }

    } catch (std::exception &e) {
        ctx.Log("error in parse body: " + static_cast<std::string>(e.what()));

        return {false, 500, "error in parse body GetMe"};
    }

    return {ok, response.status, "", {}, info};
}

BotClient::Response BotClient::GetUpdates(Ctx& ctx, std::unordered_map<std::string, int> args) {
    ctx.Add("GetUpdates: ");

    std::map<std::string, std::string> query;

    if (args.count("timeout")) {
        query["timout"] = std::to_string(args["timeout"]);
    }
    if (args.count("offset")) {
        query["offset"] = std::to_string(args["offset"]);
    }

    auto response = http_.Request(ctx, url_ + "/getUpdates", "GET", query, {}, "");

    if (response.status != 200) {
        ctx.Log("end with status " + std::to_string(response.status) +
                ", error: " + response.error);

        return {false, response.status, response.error};
    }

    bool ok;
    std::vector<Message> messages;

    try {
        ok = response.body->getValue<bool>("ok");

        if (ok) {
            auto result = response.body->getArray("result");

            for (size_t i = 0; i < result->size(); ++i) {

                auto object = result->getObject(i);
                auto message_info = object->getObject("message");

                Message message;

                message.update_id = object->getValue<int>("update_id");

                if (message_info->has("text")) {
                    message.text = message_info->getValue<std::string>("text");
                }
                if (message_info->has("message_id")) {
                    message.message_id = message_info->getValue<int>("message_id");
                }
                if (message_info->has("from")) {
                    message.from_id = message_info->getObject("from")->getValue<int>("id");
                }
                if (message_info->has("chat")) {
                    message.chat_type =
                        message_info->getObject("chat")->getValue<std::string>("type");
                    message.chat_id = message_info->getObject("chat")->getValue<int>("id");
                }

                messages.push_back(message);
            }
        }
    } catch (std::exception &e) {
        ctx.Log("error in parse body: " + static_cast<std::string>(e.what()));

        return {false, 500, "error in parse body"};
    }

    ctx.Log("success: " + std::to_string(messages.size()) + " messages");

    return {ok, response.status, "", messages};
}

BotClient::Response BotClient::SendMessage(Ctx& ctx, Message& message, std::string text) {
    ctx.Add("SendMessage: ");

    Poco::JSON::Object json_body;
    json_body.set("chat_id", message.chat_id);
    json_body.set("text", text);

    if (message.reply) {
        json_body.set("reply_to_message_id", message.reply_to_id);
    }

    std::stringstream body;
    json_body.stringify(body);

    auto response = http_.Request(ctx, url_ + "/sendMessage", "POST", {}, {}, body.str());

    if (response.status != 200) {
        ctx.Log("end with status " + std::to_string(response.status) +
                ", error: " + response.error);

        return {false, response.status, response.error};
    }

    return {true, response.status};
}

int BotClient::GetOffset() {
    offset_file_.open("offset.txt", std::ios_base::in);

    int offset = 0;

    if (offset_file_.is_open()) {
        offset_file_ >> offset;
        offset_file_.close();
    }
    return offset;
}

void BotClient::SetOffset(int offset) {
    offset_file_.open("offset.txt", std::ios_base::out | std::ios_base::trunc);

    if (offset_file_.is_open()) {
        offset_file_ << offset;
        offset_file_.close();
    }
}
