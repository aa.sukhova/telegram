#include <cstring>

#include "bot_client/inc/http.h"

Http::HttpResponse Http::Request(Ctx& ctx, std::string url, std::string method,
                                 std::map<std::string, std::string> query,
                                 std::map<std::string, std::string> headers, std::string body) {

    Poco::URI uri(url);

    for (auto [key, value] : query) {
        uri.addQueryParameter(key, value);
    }

    Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());

    Poco::Net::HTTPRequest request(method, uri.toString());

    if (!body.empty()) {
        request.setContentType("application/json");
        request.setContentLength(body.size());
    }
    for (auto [key, value] : headers) {
        request.add(key, value);
    }

    Poco::SharedPtr<Poco::JSON::Object> response_body_json;

    try {
        auto& request_body = session.sendRequest(request);

        if (!body.empty()) {
            request_body << body;
        }

    } catch (std::exception& e) {
        ctx.Log("error in session.sendRequest: " + static_cast<std::string>(e.what()));

        return {500, "error in sendRequest", response_body_json};
    }

    Poco::Net::HTTPResponse response;
    Poco::JSON::Parser parser;

    std::string error;

    try {
        std::istream& response_body = session.receiveResponse(response);

        try {

            response_body_json = parser.parse(response_body).extract<Poco::JSON::Object::Ptr>();

        } catch (std::exception& e) {
            ctx.Log("body isn't json: " + static_cast<std::string>(e.what()));

            return {400, "body isn't json", response_body_json};
        }

        if (response.getStatus() != 200) {

            std::stringstream error_stream;
            response_body_json->stringify(error_stream);
            error = error_stream.str();
        }

        ctx.Log("http.request success st: " + std::to_string(response.getStatus()));

        return {response.getStatus(), error, response_body_json};

    } catch (std::exception& e) {
        ctx.Log("error in recieveResponse: " + static_cast<std::string>(e.what()));

        return {500, "error in recieveResponse", response_body_json};
    }
}
