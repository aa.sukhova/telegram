#include "bot_client/inc/util.h"
#include <random>

std::string GenCtxId(int len) {
    std::mt19937 generator(rand() * 1000);

    std::string dict = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    std::uniform_int_distribution<> dist(0, dict.size() - 1);

    std::string gen_string;
    for (int i = 0; i < len; ++i) {
        gen_string += dict[dist(generator)];
    }

    return gen_string;
}

std::string TimeNow() {
    std::time_t now = std::time(nullptr);

    char time[200];

    if (std::strftime(time, sizeof(time), "%FT%X", std::localtime(&now))) {
        return time;
    }

    return "err time";
}
