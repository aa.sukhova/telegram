#include "bot_client/inc/ctx.h"
#include "bot_client/inc/util.h"

Ctx::Ctx() : id_(GenCtxId(10)), log_file_("../log/log0.txt") {
}

void Ctx::Add(std::string text) {
    info_.push_back(text);
}

void Ctx::Log(std::string text) {
    std::fstream f;

    f.open(log_file_, std::fstream::out | std::fstream::app);

    if (f.is_open()) {
        std::string log_string = TimeNow();

        log_string += " [" + id_ + "] ";

        for (auto& part : info_) {
            log_string += part + " ";
        }

        log_string += text + "\n";

        f << log_string;

        f.close();
        Ctx::Rotate();
    }
}

void Ctx::Rotate() {
    struct stat stat_buf;

    while (1) {
        int rc = stat(log_file_.c_str(), &stat_buf);
        if (rc) {
            break;
        }

        if (stat_buf.st_size > 1024*1024*30) {
            std::regex number(R"(\d+)");
            std::smatch match;
            std::regex_search(log_file_, match, number);
            int next = stoi(match[0]) + 1;

            log_file_ = match.prefix().str() + std::to_string(next) + ".txt";
        } else {
            break;
        }
    }
}

void Ctx::Clean() {
    id_ = GenCtxId(10);
    info_.clear();
}
