#include "bot.h"

Bot::Bot(std::string id) : client_(id), work_(false) {
}

void Bot::Start() {
    work_ = true;

    Ctx ctx;

    auto bot = client_.GetMe(ctx);

    if (!bot.ok) {
        ctx.Log("test request failed: " + bot.error);
        return;
    }

    while (work_) {
        ctx.Clean();

        int offset = client_.GetOffset();

        auto res = client_.GetUpdates(ctx, {{"offset", offset + 1}, {"timeout", 7}});

        ctx.Clean();

        if (!res.ok) {
            continue;
        }

        for (auto& msg : res.updates) {
            if (!work_) {
                break;
            }

            if (msg.text == "/stop" || msg.text == "stop") {
                Bot::Stop(ctx);
            } else {
                Bot::HelloFromBot(ctx, msg);
            }

            client_.SetOffset(msg.update_id);
        }
    }
}

void Bot::Stop(Ctx& ctx) {
    ctx.Log("stop by command from message");
    work_ = false;
}

void Bot::HelloFromBot(Ctx& ctx, BotClient::Message& msg) {
    BotClient::Message send_msg;
    send_msg.chat_id = msg.chat_id;
    send_msg.reply = true;
    send_msg.reply_to_id = msg.message_id;

    client_.SendMessage(ctx, send_msg, "Hello you!");
}
