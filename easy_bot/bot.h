#pragma once

#include "bot_client/inc/api.h"

class Bot {
public:
    Bot(std::string id);

    void Start();

    void HelloFromBot(Ctx& ctx, BotClient::Message& msg);

    void Stop(Ctx& ctx);

private:
    BotClient client_;
    bool work_;
};
