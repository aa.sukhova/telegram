# Telegram

## Info

With this API you can easily create your own bots with different functions. API uses library POCO.

API is in directory **bot_client** - _class BotClient_.

Example for use this API is in directory **easy_bot** - _class Bot_.

Realized:
-     getMe()
https://core.telegram.org/bots/api#getme
-     getUpdates()
https://core.telegram.org/bots/api#getupdates
-     sendMessage()
https://core.telegram.org/bots/api#sendmessage


Realized the **logging** and **dynamic change offset**, which is saved and taken from the local file.

## Build

Easy_bot is an example of use BotClient.

1. install library POCO

```
sudo apt-get install libpoco-dev
``` 

2. create build directory in directory telegram

```
mkdir build
cd build
cmake ..
```   

3. make the project

```
make easy_bot
```

4. run bot

```
./easy_bot
```

## Structure

#### BotClient
    
    class with client API

#### Ctx

    class for realize logging with parameters (request_id, time, name method)

#### Http

    class for realize http_request with POCO library

#### Util

    directory for speacial functions
